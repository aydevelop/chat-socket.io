'use strict';

const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', [
      {
        firstName: 'John',
        lastName: 'Doe',
        email: 'john@gmail.com',
        password: await bcrypt.hash('secret', 10),
        gender: 'male',
      },
      {
        firstName: 'Sem',
        lastName: 'Smith',
        email: 'sem@gmail.com',
        password: await bcrypt.hash('secret', 10),
        gender: 'male',
      },
      {
        firstName: 'Sara',
        lastName: 'Doe',
        email: 'sara@gmail.com',
        password: await bcrypt.hash('secret', 10),
        gender: 'male',
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    await queryInterface.bulkDelete('Users', null, {});
  },
};
