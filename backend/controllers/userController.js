const User = require('../models').User;
const sequelize = require('sequelize');

exports.update = async (req, res) => {
  let id = req.user.id;
  if (req.file) {
    req.body.avatar = req.file.filename;
  }

  if (typeof req.body.avatar !== 'undefined' && req.body.avatar.length === 0)
    delete req.body.avatar;

  try {
    const [rows, result] = await User.update(req.body, {
      where: { id },
      individualHooks: true,
    });

    result[0].password = undefined;
    const user = JSON.stringify(result[0]);
    return res.send(user);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
};
