const router = require('express').Router();
const {
  index,
  create,
  message,
  deleteChat,
} = require('../controllers/chatControllers');
const { auth } = require('../middleware/auth');

router.get('/', [auth], index);
router.get('/messages', [auth], message);
router.post('/create', [auth], create);
router.delete('/:id', [auth], deleteChat);

module.exports = router;
