const router = require('express').Router();
const { update } = require('../controllers/userController');
const { body } = require('express-validator');
const { auth } = require('../middleware/auth');
const { validate } = require('../validators');
const { rules: updateRules } = require('../validators/user/update');
const { userFile } = require('../middleware/fileUpload');
const upload = require('../middleware/uploadPhoto');

router.post(
  '/update',
  [auth, upload.single('avatar'), updateRules, validate],
  update
);

module.exports = router;
