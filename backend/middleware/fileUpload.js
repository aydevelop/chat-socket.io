const muler = require('multer');
const fs = require('fs');
const path = require('path');
const multer = require('multer');

const getFileType = (file) => {
  const mimeType = file.mimeType.split('/');
  return mimeType[mimeType.length - 1];
};

const generateFileName = (req, file, cb) => {
  const ext = getFileType(file);
  const filename =
    Date.now() + '-' + Math.round(Math.random() * 1e9) + '.' + ext;
  cb(null, filename);
};

const fileFilter = (req, file, cb) => {
  const ext = getFileType(file);
  const allowedType = '/jpeg/jpg/png';
  const passed = allowedType.test(ext);

  if (passed) {
    return cb(null, true);
  }

  return cb(null, false);
};

exports.userFile = (req, res) => {
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      const { id } = req.body;
      const dest = `uploads/user/${id}`;

      fs.access(dest, (error) => {
        if (error) {
          return fs.mkdir(dest, (error) => {
            cb(error, dest);
          });
        } else {
          fs.rmdir(dest, { recursive: true }, (error) => {
            cb(error, dest);
          });
          return cb(null, dest);
        }
      });
    },
    filename: generateFileName,
  });

  return multer({ storage, fileFilter }).single('file');
};
