const multer = require('multer');
const path = require('path');
const fs = require('fs');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const id = req.user.id;
    const dest = `uploads/user/${id}`;

    fs.access(dest, (error) => {
      if (error) {
        return fs.mkdir(dest, (error) => {
          if (error) console.log(error);
        });
      } else {
        fs.readdir(dest, function (err, items) {
          for (var i = 0; i < items.length; i++) {
            const pathRes = path.join(dest, items[i]);
            fs.unlink(pathRes, (error) => {
              if (error) console.log(error);
            });
          }
        });
      }

      return cb(null, dest);
    });
  },
  filename: function (req, file, cb) {
    cb(
      null,
      Date.now() +
        Math.round(Math.random() * 1e9) +
        path.extname(file.originalname)
    );
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({ storage, fileFilter });

module.exports = upload;
