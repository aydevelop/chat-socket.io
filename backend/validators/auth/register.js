const { body } = require('express-validator');

exports.rules = [
  body('firstName').notEmpty(),
  body('lastName').notEmpty(),
  body('email').notEmpty(),
  body('gender').notEmpty(),
  body('password').isLength({ min: 5 }),
];
