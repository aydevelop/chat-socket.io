const { body } = require('express-validator');

exports.rules = [
  body('firstName').notEmpty(),
  body('lastName').notEmpty(),
  body('gender').notEmpty(),
  body('email').notEmpty(),
  body('password').optional().isLength({ min: 5 }),
];
