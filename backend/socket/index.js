const socketIo = require('socket.io');
const { Message, sequelize } = require('./../models');

const users = new Map();
const currentUserSockets = new Map();

const SocketServer = async (server) => {
  const io = socketIo(server, {
    cors: {
      origin: '*',
    },
  });

  io.on('connection', (socket) => {
    socket.on('join', async (user) => {
      let userActiveConnections = [];

      if (users.has(user.id)) {
        const existUser = users.get(user.id);
        existUser.sockets = [...existUser.sockets, ...[socket.id]];
        users.set(user.id, existUser);
        userActiveConnections = [...existUser.sockets, ...[socket.id]];
        currentUserSockets.set(socket.id, user.id);
      } else {
        users.set(user.id, { id: user.id, sockets: [socket.id] });
        userActiveConnections.push(socket.id);
        currentUserSockets.set(socket.id, user.id);
      }

      const dbFriends3 = await getChatters(user.id);
      //const dbFriends2 = dbFriends3.filter((item) => item.id === user.id);
      const dbFriends2 = dbFriends3;
      const dbFriends = [...new Set(dbFriends2)];

      const onlineFriends = [];
      for (let i = 0; i < dbFriends.length; i++) {
        if (users.has(dbFriends[i])) {
          const cht = users.get(dbFriends[i]);

          cht.sockets.forEach((socket) => {
            try {
              io.to(socket).emit('online', dbFriends);
            } catch (error) {
              console.log('emmit1 error: ' + error);
            }
          });

          onlineFriends.push(cht.id);
        }
      }

      userActiveConnections.forEach((socket) => {
        try {
          io.to(socket).emit('friends', dbFriends);
        } catch (error) {
          console.log('emmit2 error: ' + error);
        }
      });

      io.to(socket.id).emit('typing', 'typing 28873298');
    });

    socket.on('disconnect', async () => {
      if (currentUserSockets.has(socket.id)) {
        const user = users.get(currentUserSockets.get(socket.id));

        if (user.sockets.length > 1) {
          user.sockets = user.sockets.filter((sock) => {
            if (sock !== socket.id) return true;
            currentUserSockets.delete(sock);
            return false;
          });

          users.set(user.id, user);
        } else {
          const chatters = getChatters(user.id);

          const dbFriends2 = await getChatters(user.id);
          const dbFriends = [...new Set(dbFriends2)];

          for (let i = 0; i < dbFriends.length; i++) {
            if (users.has(dbFriends[i])) {
              const cht = users.get(dbFriends[i]);

              cht.sockets.forEach((socket) => {
                try {
                  io.to(socket).emit('offline', user);
                } catch (error) {
                  console.log('emmit1 error: ' + error);
                }
              });
            }
          }

          currentUserSockets.delete(socket.id);
          users.delete(user.id);
        }
      }
    });

    socket.on('message', async (message) => {
      let sockets = [];
      if (users.has(message.fromUser.id)) {
        sockets = users.get(message.fromUser.id).sockets;
      }

      message.toUserId.forEach((id) => {
        if (users.has(id)) {
          sockets = [...sockets, ...users.get(id).sockets];
        }
      });

      try {
        const msg = {
          type: message.type,
          fromUserId: message.fromUser.id,
          chatId: message.chatId,
          message: message.message,
        };

        await Message.create(msg);

        message.User = message.fromUser;
        message.fromUserId = message.fromUser.id;
        delete message.fromUser;

        sockets.forEach((socket) => {
          io.to(socket).emit('received', message);
        });
      } catch (e) {
        console.log('error socket.on message');
        console.log(e);
      }
    });
  });
};

const getChatters = async (userId) => {
  try {
    const [results, metadata] = await sequelize.query(`
      select cu.userId from ChatUsers as cu
      inner join (
          select c.id from Chats as c
          where exists (
              select u.id from Users as u
              inner join ChatUsers on u.id = ChatUsers.userId
              where u.id = ${parseInt(userId)} and c.id = ChatUsers.chatId
          )
      ) as cjoin on cjoin.id = cu.chatId
      where "cu.userId" != ${parseInt(userId)}
  `);

    return results.length > 0 ? results.map((el) => el.userId) : [];
  } catch (e) {
    return [];
  }

  // sequelize
  //   .query(
  //     `
  //   select cu.userId from ChatUsers as cu
  //   inner join (
  //       select c.id from Chats as c
  //       where exists (
  //           select u.id from Users as u
  //           inner join ChatUsers on u.id = ChatUsers.userId
  //           where u.id = ${parseInt(userId)} and c.id = ChatUsers.chatId
  //       )
  //   ) as cjoin on cjoin.id = cu.chatId
  //   where "cu.userId" != ${parseInt(userId)}
  // `
  //   )
  //   .then(function (projects) {
  //     console.log(projects);
  //   });

  // return [];
};

module.exports = SocketServer;
