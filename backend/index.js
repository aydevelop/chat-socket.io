const config = require('./config/app');
const express = require('express');
const router = require('./router');
const cors = require('cors');
var multer = require('multer');
const http = require('http');

const app = express();
const upload = multer();
app.use(cors());
app.use(express.static('uploads'));
app.use(express.static('public'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(router);

const server = http.createServer(app);
const SocketServer = require('./socket');
SocketServer(server);

const port = config.appPort;
server.listen(port, () => {
  console.log(`Server1 listening on port ${port}`);
});
