import './App.scss';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Chat from './components/chat/Chat';
import ProtectedRoute from './components/router/ProtectedRoute';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// import { library } from '@fortawesome/fontawesome-svg-core';
// import { faCoffee, faSmile, faImage } from '@fortawesome/free-solid-svg-icons';
// library.add(faCoffee, faSmile, faImage);

function App() {
  return (
    <BrowserRouter>
      <div className='App'>
        <Switch>
          <ProtectedRoute exact path='/' component={Chat} />
          <Route exact path='/' component={Chat} />
          <Route path='/login' component={Login} />
          <Route path='/register' component={Register} />
          <Route render={() => <h1>404 Page not found</h1>} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
