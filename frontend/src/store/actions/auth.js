import AuthService from '../../services/authService';
export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';
export const LOGOUT = 'LOGOUT';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';

export const login = (params, history) => (dispatch) => {
  console.log('login...');
  return AuthService.login(params)
    .then((data) => {
      dispatch({ type: LOGIN, payload: data });
      history.push('/');
    })
    .catch((err) => {});
};

export const register = (params, history) => (dispatch) => {
  console.log('params: ' + JSON.stringify(params));
  return AuthService.register(params)
    .then((data) => {
      dispatch({ type: REGISTER, payload: data });
      history.push('/');
    })
    .catch((err) => {});
};

export const logout = () => (dispatch) => {
  AuthService.logout();
  dispatch({ type: LOGOUT });
};

export const updateProfile = (params) => (dispatch) => {
  return AuthService.updateProfile(params)
    .then((data) => {
      try {
        localStorage.setItem('user', JSON.stringify(data));
        dispatch({ type: UPDATE_PROFILE, payload: data });
      } catch (err) {
        alert(err);
      }
    })
    .catch((err) => {});
};
