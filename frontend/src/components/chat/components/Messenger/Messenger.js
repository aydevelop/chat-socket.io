import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import './Messenger.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

import {
  faAddressBook,
  faBaby,
  faCaretDown,
} from '@fortawesome/free-solid-svg-icons';

const Messenger = () => {
  const chat = useSelector((state) => state.chatReducer.currentChat);
  const chats = useSelector((state) => state.chatReducer.chats);
  const user = useSelector((state) => state.authReducer.user);
  const socket = useSelector((state) => state.chatReducer.socket);

  useEffect(() => {}, [chats]);

  const [message, setMessage] = useState('');
  const [image, setImage] = useState('');

  const handleMessage = (e) => {
    const value = e.target.value;
    setMessage(value);

    //notify
  };

  const handleKeyDown = (e, imageUpload) => {
    console.log('key: ' + e.key);
    if (e.key == 'Enter') {
      console.log('key enter');
      sendMessage(imageUpload);
    }
  };

  const sendMessage = (imageUpload) => {
    if (message.length < 1 && !imageUpload) return;

    const msg = {
      type: imageUpload ? 'image' : 'text',
      fromUserId: user.id,
      fromUser: user,
      toUserId: chat.Users.map((user) => user.id),
      chatId: chat.id,
      message: imageUpload ? image : message,
    };

    setMessage('');
    setImage('');
    socket.emit('message', msg);

    //send message wih socket
  };

  const activeChat = () => {
    return Object.keys(chat).length > 0;
  };

  return (
    <div id='messenger' className='shadow-light'>
      {activeChat() ? (
        <div className='bodymessenger'>
          {/* <p>CHAT: {JSON.stringify(chat)}</p> */}

          <div className='chat-header'>
            <div>
              <b>Current chat</b>
            </div>
            <div>
              <a href='#'>Add user to chat</a>
              &nbsp;&nbsp;&nbsp;
              <a href='#'>Leave chat</a>
              &nbsp;&nbsp;&nbsp;
              <a href='#'>Delete chat</a>
            </div>
          </div>

          {/*___________________________ USERS ___________________________*/}
          <div className='bmsg'>
            <center>
              <div>
                <big>
                  <strong>Users</strong>
                </big>
              </div>
            </center>
            {chat.Users.map((user) => {
              return (
                <div key={user.id}>
                  <hr />
                  {/* <div
                    className={
                      user.status != 'online' ? 'color-red' : 'color-green'
                    }
                  >
                    {user.firstName} {user.lastName} <span>(Status - </span>
                    {user.status.toUpperCase()})
                  </div> */}
                  <div>
                    {user.firstName} {user.lastName}
                  </div>
                  <img className='chat-avatar' src={user.avatar} />
                  <hr />
                </div>
              );
            })}
          </div>

          {/*___________________________ Messages ___________________________*/}
          <div className='bmsg'>
            <center>
              <div>
                <big>
                  <strong>Messages</strong>
                </big>
              </div>
            </center>
            <hr />
            {chat.Messages.map((msg) => {
              return (
                <div key={msg.id}>
                  {msg.fromUserId == user.id ? (
                    <div className='owner'>
                      <span className='strong'>{msg.message}</span>
                      <hr />
                    </div>
                  ) : (
                    <div key={msg.id} className='other-persone'>
                      <div>
                        {' '}
                        {msg.User.firstName} {msg.User.lastName} (
                        {msg.User.email}):
                      </div>
                      <p>{msg.message}</p>
                      <hr />
                    </div>
                  )}
                </div>
              );
            })}

            <div className='chat-msg'>
              <input
                onChange={(e) => handleMessage(e)}
                onKeyDown={(e) => handleKeyDown(e, false)}
                type='text'
                placeholder='Message...'
                value={message}
              />
              <button>
                Send Message
                <FontAwesomeIcon
                  style={{ marginLeft: 10 }}
                  color='white'
                  icon={faEnvelope}
                />
              </button>
            </div>
          </div>
        </div>
      ) : (
        <p>No active chat</p>
      )}
    </div>
  );
};

export default Messenger;
