import React, { Fragment, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { logout, login } from '../../../../store/actions/auth';
import './Navbar.scss';
import Modal from '../../../modal/Modal';
import { updateProfile } from '../../../../store/actions/auth';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';

const Navbar = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.authReducer.user);
  const [showOptions, setShowOptions] = React.useState(false);
  const [showModal, setShowModal] = React.useState(false);

  const [firstName, setFirstName] = useState(user.firstName);
  const [lastName, setLastName] = useState(user.lastName);
  const [email, setEmail] = useState(user.email);
  const [gender, setGender] = useState(user.gender);
  const [avatar, setAvatar] = useState(null);
  const [password, setPassword] = useState(user.password);

  const submitForm = (e) => {
    e.preventDefault();
    const form = { firstName, lastName, email, gender, avatar, password };
    const formData = new FormData();

    if (!password || password.trim() == 0) {
      delete form.password;
    }

    if (!avatar) {
      delete form.avatar;
    }

    for (const key in form) {
      formData.append(key, form[key]);
    }

    dispatch(updateProfile(formData));
    setShowOptions(false);
  };

  return (
    <div id='navbar'>
      <div>
        <h2>Chat.io</h2>
      </div>
      <div id='profile-menu'>
        <img width='50' height='50' src={user.avatar} alt='Avatar' />
        <div
          onClick={() => {
            setShowOptions(!showOptions);
          }}
          style={{ cursor: 'pointer', userSelect: 'none' }}
          className='login'
        >
          <b>{user.firstName}</b>
          <FontAwesomeIcon icon={faCaretDown} />
        </div>
        {showOptions && (
          <div>
            <p>
              <a href='#'>
                <b
                  onClick={(e) => {
                    e.preventDefault();
                    setShowModal(!showModal);
                  }}
                >
                  Update profile
                </b>
              </a>
            </p>
            <p>
              <a href='#'>
                <b
                  onClick={() => {
                    dispatch(logout());
                  }}
                >
                  Logout
                </b>
              </a>
            </p>
          </div>
        )}
        {showOptions && showModal && (
          <Modal>
            <Fragment key='header'></Fragment>
            <Fragment key='body'>
              <div>
                <input
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                  placeholder='First name'
                />
              </div>
              <div>
                <input
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  name=''
                  placeholder='Last name'
                />
              </div>
              <div>
                <input
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  name=''
                  placeholder='Email'
                />
              </div>
              <div>
                <input
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  name=''
                  placeholder='Password'
                />
              </div>
              <div>
                <input
                  value={gender}
                  onChange={(e) => setGender(e.target.value)}
                  name=''
                  placeholder='Gender'
                />
              </div>
              <div style={{ width: '50px' }}>
                <input
                  placeholder='Avatar'
                  type='file'
                  onChange={(e) => setAvatar(e.target.files[0])}
                />
              </div>
            </Fragment>
            <Fragment key='footer'>
              <a
                onClick={(e) => {
                  submitForm(e);
                }}
                style={{
                  display: 'block',
                  marginTop: '5px',
                  fontWeight: 'bold',
                }}
                href='#'
              >
                Update Profile
              </a>
            </Fragment>
          </Modal>
        )}
      </div>
    </div>
  );
};

export default Navbar;
