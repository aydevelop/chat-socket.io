import React from 'react';
import './FriendList.scss';
import { useSelector, useDispatch } from 'react-redux';
import Friend from '../Friend/Friend';
import { setCurrentChat } from '../../../../store/actions/chat';

const FriendList = () => {
  const dispatch = useDispatch();
  const chats = useSelector((state) => state.chatReducer.chats);

  const openChat = (chat) => {
    dispatch(setCurrentChat(chat));
  };

  return (
    <div id='friends'>
      <div id='title'>
        <br />
        <center className='all-chat-title'>
          <div>All Chats</div>
        </center>
        <h3 className='m-0'>
          Friends{' '}
          <small>
            <button>ADD NEW</button>
          </small>
        </h3>

        <div id='friends-box'>
          {chats.length ? (
            chats.map((chat) => {
              return (
                <Friend
                  click={() => {
                    openChat(chat);
                  }}
                  chat={chat}
                  key={chat.id}
                />
              );
            })
          ) : (
            <p id='no-chat'>No friends added</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default FriendList;
