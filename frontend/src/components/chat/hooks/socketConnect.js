import { useEffect } from 'react';
import socketIOClient from 'socket.io-client';
import {
  onlineFriend,
  onlineFriends,
  setSocket,
} from '../../../store/actions/chat';

function useSocket(user, dispatch) {
  useEffect(() => {
    const socket = socketIOClient.connect('http://127.0.0.1:3001');
    socket.emit('join', user);

    dispatch(setSocket(socket));

    socket.on('typing', (user) => {
      console.log('Event typing', user);
    });

    socket.on('friends', (friends) => {
      //console.log('friends:: ', friends);
    });

    socket.on('online', (user) => {
      //console.log('online:: ', user);
      dispatch(onlineFriends(user));
    });

    socket.on('offline', (user) => {
      //console.log('offline:: ', user);
    });

    socket.on('received', (message) => {
      console.log('received ' + JSON.stringify(message));
    });
  }, [dispatch]);
}

export default useSocket;
