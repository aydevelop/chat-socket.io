import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Navbar from './components/Navbar/Navbar';
import { fetchChats } from '../../store/actions/chat';
import FriendList from './components/FriendList/FriendList';
import Messenger from './components/Messenger/Messenger';
import './Chat.scss';
import useSocket from './hooks/socketConnect';

const Login = () => {
  const user = useSelector((state) => state.authReducer.user);
  const dispatch = useDispatch();

  useSocket(user, dispatch);
  useEffect(() => {
    dispatch(fetchChats())
      .then((res) => {
        console.log('CHATS: ' + JSON.stringify(res));
      })
      .catch();
  }, [dispatch]);

  return (
    <div id='chat'>
      <div>
        {/* <h1>Chat screen</h1>
        <p>Welcome, {user.firstName}</p> */}
        <Navbar />
        <div id='chat-wrap' className='border'>
          <div id='chat-menu'>
            <FriendList />
          </div>
          <div id='chat-content'>
            <Messenger />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
