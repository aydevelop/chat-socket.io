import React from 'react';
import './Modal.scss';

const Modal = (props) => {
  const findByKey = (name) => {
    return props.children.map((child) => {
      if (child.key === name) return child;
    });
  };

  return (
    <div className='modal01'>
      <div>{findByKey('header')}</div>
      <div>{findByKey('body')}</div>
      <div>{findByKey('footer')}</div>
    </div>
  );
};

export default Modal;
