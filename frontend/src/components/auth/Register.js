import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { useDispatch } from 'react-redux';
import { register } from '../../store/actions/auth';

const Register = (props) => {
  const dispatch = useDispatch();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [gender, setGender] = useState('');
  const [password, setPassword] = useState('');

  const submitForm = (e) => {
    e.preventDefault();
    dispatch(
      register({ firstName, lastName, email, gender, password }, props.history)
    );
  };

  return (
    <div id='auth-container'>
      <div id='auth-card'>
        <div>
          <div id='image-secion'>
            <div>
              <h2>Create an account</h2>
            </div>
          </div>
          <div id='form-secion'>
            <form className='mb-1' onSubmit={submitForm}>
              <div className='input-field mb-1'>
                <input
                  required='required'
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                  placeholder='First Name'
                />
              </div>
              <div className='input-field mb-1'>
                <input
                  required='required'
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  placeholder='Last Name'
                />
              </div>
              <div className='input-field mb-1'>
                <input
                  required='required'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder='Email'
                />
              </div>
              <div className='input-field mb-2'>
                <select
                  required='required'
                  value={gender}
                  onChange={(e) => setGender(e.target.value)}
                >
                  <option value='male'>Male</option>
                  <option value='female'>Female</option>
                </select>
              </div>
              <div className='input-field mb-1'>
                <input
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder='Password'
                  required='required'
                />
              </div>
              <button>Register</button>
            </form>
            <p>
              Already have an account? <Link to='/login'>Login</Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
