import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import AuthService from '../../services/authService';

import { useDispatch } from 'react-redux';
import { login } from '../../store/actions/auth';

const Login = (props) => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const submitForm = (e) => {
    e.preventDefault();
    dispatch(login({ email, password }, props.history));
    // AuthService.login({ email, password }).then((res) => console.log(res));
  };

  return (
    <div id='auth-container'>
      <div id='auth-card'>
        <div>
          <div id='image-secion'>
            <div>
              <h2>Login</h2>
            </div>
          </div>
          <div id='form-secion'>
            <form onSubmit={submitForm}>
              <div className='input-field mb-1'>
                <input
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                  required='required'
                  type='text'
                  placeholder='Email'
                />
              </div>
              <div className='input-field mb-2'>
                <input
                  onChange={(e) => setPassword(e.target.value)}
                  value={password}
                  required='required'
                  type='password'
                  placeholder='Password'
                />
              </div>
              <button>Login</button>
            </form>
            <p>
              Don't have an account? <Link to='/register'>Register</Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
