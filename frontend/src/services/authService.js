import API from './api';

const AuthService = {
  login: (data) => {
    return API.post('/login', data)
      .then((res) => {
        setHeaderAndStorage(res.data);

        return res.data;
      })
      .catch((err) => {
        console.log('AuthService.login error: ' + err);
        throw err;
      });
  },
  register: (data) => {
    return API.post('/register', data)
      .then((res) => {
        setHeaderAndStorage(res.data);
        return res.data;
      })
      .catch((err) => {
        console.log('AuthService.register error: ' + err);
        throw err;
      });
  },
  logout: () => {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
  },
  updateProfile: (data) => {
    const headers = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };

    return API.post('/users/update', data, headers)
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        console.log('AuthService.updateProfile error: ' + err);
        throw err;
      });
  },
};

const setHeaderAndStorage = (user) => {
  localStorage.setItem('token', user.token);
  localStorage.setItem('user', JSON.stringify(user));
};

export default AuthService;
