import axios from 'axios';
import store from '../store';
import { logout } from '../store/actions/auth';

const api = axios.create({
  headers: {
    Accept: 'applicattion/json',
  },
  baseURL: 'http://127.0.0.1:3001',
});

api.interceptors.request.use(
  (request) => {
    const token = store.getState()?.authReducer?.token;
    if (token) {
      request.headers['Authorization'] = `Bearer ${token}`;
    }

    return request;
  },
  (error) => {
    throw error;
  }
);

api.interceptors.response.use(
  (response) => {
    console.log('response: ' + response);
    return response;
  },
  (error) => {
    if (error.response.status == 401) {
      //store.dispatch(logout());
    }
    throw error;
  }
);

export default api;
